package AddAbstractPro;

	import java.util.Scanner;

	abstract class ClassA {

	    int a, b, sum;

	    abstract void input();
	    abstract void add();
	    abstract void result();
	    Scanner sc = new Scanner(System.in);
	}

	class Main extends ClassA{

	    void input() {
	        System.out.print("Enter Two Numbers :");
	        a = sc.nextInt();
	        b = sc.nextInt();
	    }

	    void add() {
	        sum = a + b;
	    }

	    void result() {
	        System.out.print("The Sum of Two Numebrs is :" + sum);
	    }

	    public static void main(String args[]) {
	        Main st = new Main();
	        st.input();
	        st.add();
	        st.result();
	    }
	}

